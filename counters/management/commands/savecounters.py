# -*- coding: utf-8 -*-
from collections import defaultdict

from django.core.management.base import BaseCommand, CommandError
from django.db.models import get_model

from counters import redis_conns


class FieldSubstitude(object):
    """
    Подмена для поля.
    Будем передавать это вместо обычного поля модели в queryset._update.
    HACK: На данный моммент compiler использует только атрибут column
          и метод get_db_prep_save если значение не может само себя приготовить
    """
    def __init__(self, field_name):
        self.column = field_name

class IncrExpression(object):
    """
    Выражение прибавления числа. Сильно урезанная версия джанговских F() - выражений.
    Зато никаких проверок на существование поля.
    """
    def __init__(self, field_name, incr):
        self.column, self.incr = field_name, incr

    def prepare_database_save(self, field):
        return self

    def as_sql(self, qn, connection):
        return '%s + %%s' % qn(self.column), [self.incr]


class Command(BaseCommand):
    help = 'Saving counters from redis to database'

    def handle(self,  **options):
        # NOTE: массового использования keys можно избежать переписав весь пакет на редисовые хеши,
        #       к сожадению хеши пока только в нестабильных редисах
        redis_conn = redis_conns['old']

        keys = redis_conn.keys('*')
        if isinstance(keys, str):
            keys = keys.split()
        incr_keys = filter(lambda k: k.endswith('.incr'), keys)

        if not incr_keys:
            return

        # NOTE: на самом деле это нужно делать в транзакции, но они только в нестабильных редисах
        pipe = redis_conn.pipeline(transaction=False)
        pipe.mget(incr_keys)
        # Убиение не инкрементальных ключей никак не может привести к потере данных, поэтому сносим их всё
        # Инкременты мы только что получили, считаем, что они не изменились
        pipe.delete(*keys)
        data = pipe.execute()[0]

        # Собираемся построить структуру:
        #   имя_модели -> имя_поля -> инкремент -> [id1, id2, ...]
        # т.е. группируем по модели, полю и значению инкремента
        incrs = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))

        for i, key in enumerate(incr_keys):
            model_name, pk, field_name = key.split(':')
            field_name = field_name.split('.')[0]
            incrs[model_name][field_name][int(data[i])].append(pk)

        # Выполним массовые апдейты для каждой группы модель - поле - инкремент в стиле
        #   update модель set поле = поле + инкремент where id in (список id)
        for model_name, field_incrs in incrs.iteritems():
            model = get_model(*model_name.split('.'))
            for field_name, grouped_incrs in field_incrs.iteritems():
                # Создаём подмену поля
                field = FieldSubstitude(field_name)
                for incr, pks in grouped_incrs.iteritems():
                    # Вызываем низкоуровневый update,
                    # передаём ему типа поле и типа выражение
                    update_tuple = (field, model, IncrExpression(field_name, incr))
                    qs = model.objects.filter(pk__in=pks)
                    qs._for_write = True # Необходимо, чтобы разруливалось к мастеру
                    qs._update([update_tuple])
