# -*- coding: utf-8 -*-
from collections import defaultdict
from operator import concat

from django.core.management.base import BaseCommand, CommandError
from django.db.models.loading import cache

from counters import redis_conn


class Command(BaseCommand):
    help = 'Migrates counters from database to redis. Call after savecounters'

    def handle(self,  **options):
        verbosity = int(options.get('verbosity', 1))
        cache._populate()

        for app, models in cache.app_models.items():
            for model, cls in models.items():
                if hasattr(cls, '_counters'):
                    for field_name in cls._counters:
                        if app in ('cars', 'moto') and model == 'model':
                            pass
                        elif model == 'modelfederaldistricthits':
                            self.migrate_combos(cls, field_name, verbosity)
                        else:
                            self.migrate_hits(cls, field_name, verbosity)

    def migrate_hits(self, cls, field_name, verbosity=1):
        key = getattr(cls, 'key_for_' + field_name)()

        data = cls.objects.extra(where=["%s > 0" % field_name], select={field_name: field_name}) \
                          .values_list('pk', field_name)

        if verbosity >= 1:
            print 'Starting %s - %d records' % (key, len(data))

        pipe = redis_conn.pipeline(transaction=False)
        for pk, cnt in data:
            pipe.zincrby(key, pk, cnt)
        pipe.execute()

    def migrate_combos(self, cls, field_name, verbosity=1):
        model_cls = cls.model.field.rel.to
        key_func = getattr(model_cls, 'key_for_' + field_name)

        # Мигрируем основные счётчики
        data = cls.objects.extra(where=["%s > 0" % field_name], select={field_name: field_name}) \
                          .values_list('model', 'district', field_name)

        if verbosity >= 1:
            print 'Starting %s - %d records' % (key_func(), len(data))

        pipe = redis_conn.pipeline(transaction=False)
        for pk, district, cnt in data:
            key = key_func(district)
            pipe.zincrby(key, pk, cnt)
        pipe.execute()

        # Мигрируем снепшоты
        for period in ('day', 'week', 'month'):
            field = getattr(model_cls, 'model%s%s' % (field_name, period))
            snapshot_cls = field.related.model

            dates = snapshot_cls.objects.values_list('date_counted', flat=True).distinct().order_by('-date_counted')[:3]
            districts = range(1, 8)
            for district_id in districts:
                key = key_func(district_id)
                snapshot_key = '%s:shot.%s' % (key, period)
                last_top_key = '%s:top.%s.last' % (key, period)
                prev_top_key = '%s:top.%s.prev' % (key, period)

                for i, date in enumerate(dates):
                    shot_data = snapshot_cls.objects.filter(date_counted=date, district=district_id, hits__gt=0) \
                                            .values_list('model', 'hits')
                    print district_id, date, len(shot_data)

                    tmp_snapshot_key = snapshot_key + str(i)
                    pipe = redis_conn.pipeline(transaction=False)
                    for pk, hits in shot_data:
                        pipe.zincrby(tmp_snapshot_key, pk, hits)
                    pipe.execute()

                self.maketop(last_top_key, snapshot_key + '0', snapshot_key + '1')
                self.maketop(prev_top_key, snapshot_key + '1', snapshot_key + '2')

                redis_conn.rename(snapshot_key + '0', snapshot_key)
                redis_conn.delete(snapshot_key + '1', snapshot_key + '2')


    def maketop(self, top_key, new_snapshot, old_snapshot):
        redis_conn.zunionstore(top_key, {new_snapshot: 1, old_snapshot: -1})
        redis_conn.zremrangebyrank(top_key, 0, -101)
