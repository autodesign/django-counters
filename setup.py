from setuptools import setup, find_packages

setup(
    name='django-counters',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    url='https://bitbucket.org/metadesign/django-counters',
    license='',
    author='',
    author_email='',
    description='',
    install_requires=(
        'redis',
        'django',
    )
)
